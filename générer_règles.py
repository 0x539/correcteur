#!/usr/bin/env python3

# Auteur : Corentin Bocquillon <corentin@nybble.fr> 2018

# Ce logiciel est régi par la licence CeCILL soumise au droit français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
# sur le site « http://www.cecill.info ».

# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.

# A cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement,
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
# pris connaissance de la licence CeCILL, et que vous en avez accepté les
# termes.

# Générateur de règles pour le correcteur d’erreurs.

import getopt
import sys
import locale


def lire_arguments(arguments):
    optionsl = ['type=', 'sortie=', 'aide', 'étiquette=', 'motif=',
                'motif-précédent=', 'motif-suivant=', 'étiquettes=',
                'mot=', 'help', 'version']
    listeopt, arguments = getopt.getopt(arguments, 's:m:ahv', optionsl)

    return listeopt

def afficher_version():
    print('{} 0.1'.format(sys.argv[0]))


def afficher_aide():
    print('\nUtilisation : {} OPTION…'.format(sys.argv[0]))
    print('Générateur de règles.\n')

    print('Les arguments pour les options courtes sont les mêmes que ceux')
    print('pour les options longues.\n')

    print('--help, --aide, -h, -a')
    print('              afficher cette aide et quitter')
    print('--version, -v afficher la version et quitter')
    print('--étiquette=ÉTIQUETTE')
    print('              spécifier l’étiquette souhaité pour le mot dont ')
    print('                l’étiquette est à corriger')
    print('--motif=MOTIF, -m')
    print('              spécifier le motif du mot')
    print('--mot=MOT     spécifier le mot pour lequel on souhaite ')
    print('                changer l’étiquette.')
    print('--sortie=FICHIER, -s')
    print('              spécifier le fichier de sortie')
    print('--motif-précédant=MOTIF')
    print('              spécifier le motif précédant celui spécifié')
    print('                par --motif')
    print('--motif-suivant=MOTIF')
    print('              spécifier le motif devant suivre celui spécifié')
    print('                par --motif')
    print('--étiquettes=POSITION1:ÉTIQUETTE1;POSITION2:ÉTIQUETTE2…')
    print('              spécifier les étiquettes à donner aux mots')
    print('                donnant leur position, par exemple :')
    print('                2:étiquette;0:étiquette')

def traiter_arguments(listeoptions):
    dico = {}
    n = None

    for o, a in listeoptions:
        if o in ('-h', '--help', '-a', '--aide'):
            afficher_aide()
            exit(0)
        elif o in ('-v', '--version'):
            afficher_version()
            exit(0)
        elif o in ('--étiquette', '-é'):
            dico['étiquette'] = a
        elif o in ('--motif', '-m'):
            dico['motif'] = a
        elif o in ('-s', '--sortie'):
            dico['sortie'] = a
        elif o == '--motif-précédent':
            dico['motif-précédent'] = a
        elif o == '--motif-suivant':
            dico['motif-suivant'] = a
        elif o == '--étiquettes':
            dico['étiquettes'] = a
        elif o == '--mot':
            dico['mot'] = a

    return dico


def obtenir_fichier_sortie(options):
    if 'sortie' not in list(options.keys()) or options['sortie'] == '-':
        return sys.stdout
    else:
        return open(options['sortie'], 'a')


def obtenir_position_et_étiquettes(chaîne):
    couples = chaîne.split(';')

    couple0 = couples[0].split(':')
    if len(couple0) != 2:
        afficher_aide()
        exit(1)

    positions = couple0[0]
    étiquettes = couple0[1]

    for couple in couples[1:]:
        éléments = couple.split(':')
        if len(éléments) != 2:
            afficher_aide()
            exit(1)

        positions += ';' + éléments[0]
        étiquettes += ';' + éléments[1]

    return (positions, étiquettes)


def obtenir_règle(options):
    clefs = list(options.keys())

    if 'mot' in clefs and 'étiquette' in clefs:
        return '1\t{}\t{}\n'.format(options['mot'], options['étiquette'])
    elif 'motif' not in clefs:
        afficher_aide()
        exit(1)
    elif 'étiquette' in clefs:
        if 'motif-précédent' in clefs or 'motif-suivant' in clefs\
           or 'étiquettes' in clefs:
            print('Avec l’option --étiquette, il ne doit pas y avoir :')
            print('--motif-précédent, --motif-suivant ou --étiquettes')
            afficher_aide()
            exit(1)
        else:
            return '2\t{}\t*\t{}\n'.format(options['motif'], options['étiquette'])
    elif 'étiquettes' not in clefs:
        print('L’option --étiquette ou --étiquettes est obligatoire.')
        afficher_aide()
        exit(1)
    elif 'motif-précédent' in clefs or 'motif-suivant' in clefs:
        motif_précédent = '' if 'motif-précédent' not in clefs\
            else options['motif-précédent']
        motif_suivant = '' if 'motif-suivant' not in clefs\
            else options['motif-suivant']
        motif = options['motif']

        positions,étiquettes = obtenir_position_et_étiquettes\
            (options['étiquettes'])

        return '3\t{}\t{}\t{}\t{}\t{}\n'.format(motif_précédent, motif,
                                                motif_suivant, positions,
                                                étiquettes)
    else:
        positions,étiquettes = obtenir_position_et_étiquettes\
            (options['étiquettes'])

        return '2\t{}\t{}\t{}\n'.format(options['motif'], positions,
                                        étiquettes)

def principale(arguments):
    locale.setlocale(locale.LC_ALL, '')
    listeoptions = lire_arguments(arguments[1:])
    résultat_traitement = traiter_arguments(listeoptions)

    fichier_sortie = obtenir_fichier_sortie(résultat_traitement)
    fichier_sortie.write(obtenir_règle(résultat_traitement))


if __name__ == '__main__':
    principale(sys.argv)
