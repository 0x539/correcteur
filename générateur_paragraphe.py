# Auteur : Corentin Bocquillon <corentin@nybble.fr> 2018

# Ce logiciel est régi par la licence CeCILL soumise au droit français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
# sur le site « http://www.cecill.info ».

# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.

# À cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement,
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
# pris connaissance de la licence CeCILL, et que vous en avez accepté les
# termes.


class GénérateurParagraphe:
    def __init__(self, liste_mots):
        self.liste_mots = liste_mots
        self.dictionnaire_position_mot = {}
        self.chaîne = self.concaténer_mots(liste_mots)

    def concaténer_mots(self, liste):
        chaîne = None
        i = 0
        for ligne in liste:
            if ligne != '':
                chaîne = '\n' if ligne == '\n' else '{}'.format(ligne.split()[0])
                self.dictionnaire_position_mot['{};{}'.format\
                                               (0, len(chaîne) - 1)] = i
                break
            else:
                i += 1

        if chaîne is None:
            raise RuntimeError('Il n’y a aucune ligne correcte.')

        i += 1
        for ligne in liste[i:]:
            if ligne == '':
                i += 1
                continue
            elif ligne == '\n':
                self.dictionnaire_position_mot['{};{}'.format\
                                               (len(chaîne),
                                                len(chaîne) + len('\n') - 1)] = i
                chaîne += '\n'
            else:
                éléments = ligne.strip('\n').split()
                self.dictionnaire_position_mot['{};{}'.format\
                                               (len(chaîne),
                                                len(chaîne) \
                                                + len(éléments[0]) - 1)] = i
                chaîne += ' {}'.format(éléments[0])
            i += 1

        return chaîne

    def obtenir_chaîne(self):
        return self.chaîne

    def obtenir_position_mot_dans_liste(self, position):
        clefs = list(self.dictionnaire_position_mot.keys())
        for clef in clefs:
            clef_tmp = clef.split(';')
            clef_liste = []
            for élément in clef_tmp:
                clef_liste.append(int(élément))

            dans_clef = position >= clef_liste[0] and position <= clef_liste[1]
            if dans_clef:
                return self.dictionnaire_position_mot[clef]
        return None
