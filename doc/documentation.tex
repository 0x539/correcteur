\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage[french]{babel}
\usepackage{fullpage}
\usepackage{verbatim}
\usepackage{tabulary}
\usepackage{listings}

\date{\today}
\title{Documentation}

\lstset{
  extendedchars=true,
  inputencoding=utf8,
  literate={è}{{\`e}}1 {à}{{\`a}}1 {é}{{\'e}}1,
}

\begin{document}

\maketitle

\section{À quoi ressemble le fichier à corriger ?}

Le fichier à corriger doit être un fichier ayant deux colonnes séparés
par une tabulation. La première colonne contient un mot et la deuxième
colonne contient l’étiquette associé à ce mot.

\begin{table}[h]
  \caption{Format du fichier à corriger}
  \begin{center}
    \begin{tabulary}{0.75\textwidth}{| c | c |}
      \hline
      mot1 & étiquette \\
      mot2 & étiquette \\
      \hline
    \end{tabulary}
  \end{center}
\end{table}

\section{Utilisation du programme de correction}

Pour corriger le fichier \emph{fichier-à-corriger} avec le fichier de
règles \emph{fichier-règles} et en écrivant le résultat dans
\emph{fichier-sortie}, il faut lancer la commande suivante :

\begin{lstlisting}[language=bash]
  $ correcteur.py -r fichier-règles -s fichier-sortie -c fichier-à-corriger
\end{lstlisting}

Il est possible d’ignorer la casse ou de considérer les mots liés par
un trait d’union comme des mots séparés par des espaces.

Pour ignorer la casse il suffit d’ajouter l’option :
\begin{lstlisting}[language=bash]
  --sans-casse
\end{lstlisting}

et pour ignorer les traits d’union, il faut utiliser l’option :
\begin{lstlisting}[language=bash]
  --ignorer-traits-union
\end{lstlisting}

S’il faut observer plusieurs mots pour appliquer une des règles, il
faut utiliser l’option \emph{-n nombre-de-mots} pour spécifier le
nombre de mots qui seront observés simultanément pour appliquer les
règles.

\begin{table}[h]
  \caption{Format du dictionnaire d’entités nommées}
  \begin{center}
    \begin{tabulary}{0.75\textwidth}{| l |}
      \hline
      groupe de mots => étiquette\\
      mot => étiquette\\
      \ldots \\
      \hline
    \end{tabulary}
  \end{center}
\end{table}

Pour lui donner un dictionnaire d’entité nommées, il faut utiliser
l’option \emph{-d dictionnaire}.

\section{Utilisation du programme de génération de règles}

Si l’on souhaite générer les règles plutôt que de les écrire à la
main, il est possible d’utiliser le programme \emph{générer\_règles}.\\

Pour générer une règle de type 1 :

\begin{lstlisting}[language=bash]
  $ générer_règles.py --mot 'mot' --étiquette 'étiquette'
\end{lstlisting}

Pour générer une règle de type 2 qui change l’étiquette de tous les
mots :

\begin{lstlisting}[language=bash]
  $ générer_règles.py --motif 'un motif' --étiquette 'étiquette'
\end{lstlisting}

Pour générer une règle de type 2 qui change l’étiquette d’un seul
mot :

\begin{lstlisting}[language=bash]
  $ générer_règles.py --motif 'un groupe de mots' --étiquettes '0:étiquette;2:O'
\end{lstlisting}

Pour générer une règle de type 3 :

\begin{lstlisting}[language=bash]
  $ générer_règles.py --motif-précédent 'un groupe' --motif 'de'\
  --motif-suivant 'mots' --étiquettes '0:O'
\end{lstlisting}

\section{Format des règles}

Ce fichier décrit le format utilisé par les règles permettant la
correction des erreurs de prédictions d’étiquettes.

\subsection{À quoi ressemble un fichier de règles ?}

Un fichier de règles contient trois colonnes ou plus suivant le type
de règle. La première contient le type de règle pour sa ligne, la
dernière colonne contient l’étiquette qui remplacera l’étiquette
précédente.

\subsection{Quels sont les types de règles ?}

Il y a plusieurs types de règles, chacun de ces types est décrit dans
une sous section. Il est à noter que les colonnes des règles sont
séparées par des tabulations, la première colonne doit contenir le
type de la règle.

\subsubsection{Type 1}

Ce type de règle permet de modifier l’étiquette d’un mot correspondant à un
motif.

\begin{table}
  \caption{Exemple d’une règle de type 1}
  \begin{center}
    \begin{tabulary}{0.75\textwidth}{| c | c | c |}
      \hline
      1 & motif & étiquette \\
      \hline
    \end{tabulary}
  \end{center}
\end{table}

\subsubsection{Type 2}

Ce type de règle permet de modifier l’étiquette d’un ou plusieurs mots
dans un groupe de mots. La position des mots commencent par zéro. Les
position des mots et les étiquettes sont séparés par des
points-virgules. La troisième colonne comprend la position des mots à
changer et la quatrième les étiquettes à donner dans l’ordre des
positions de la colonne précédente. Dans le tableau~\ref{exemple type
  2}, \emph{motif} et \emph{groupe} auront les étiquettes
\emph{étiquette1} et \emph{étiquette2} respectivement.

\begin{table}
  \caption{Exemple d’une règle de type 2}
  \begin{center}
    \begin{tabulary}{0.75\textwidth}{| c | c | c | c |}
      \hline
      2 & motif d’un groupe de mots & \verb|0;2| & étiquette\verb|1;|étiquette2 \\
      \hline
    \end{tabulary}
  \end{center}
  \label{exemple type 2}
\end{table}

\subsubsection{Type 3}

Ce type de règle peut servir si le nombre de mots devant précéder ou
suivre un motif est inconnu.

\begin{table}
  \caption{Exemple d’une règle de type 3}
  \begin{center}
    \begin{tabulary}{0.75\textwidth}{| c | c | c | c | c | c |}
      \hline
      3 & motif précédant & motif & motif suivant & 0 & étiquette1 \\
      \hline
    \end{tabulary}
  \end{center}
  \label{exemple type 2}
\end{table}

\pagebreak

\tableofcontents

\end{document}
