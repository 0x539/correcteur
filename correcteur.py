#!/usr/bin/env python3

# Auteur : Corentin Bocquillon <corentin@nybble.fr> 2018

# Ce logiciel est régi par la licence CeCILL soumise au droit français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
# sur le site « http://www.cecill.info ».

# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.

# A cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement,
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
# pris connaissance de la licence CeCILL, et que vous en avez accepté les
# termes.

# Correcteur d’erreurs d’annotation automatique.

import getopt
import sys
import locale
import re
from tempfile import TemporaryFile
from générateur_paragraphe import GénérateurParagraphe
from multiprocessing import Pool
import time
from os import cpu_count


def lire_arguments(arguments):
    optionsl = ['règles=', 'fichier-à-corriger=', 'sortie=', 'aide',
                'dictionnaire', 'sans-casse', 'ignorer-traits-union',
                'help', 'version']
    listeopt, arguments = getopt.getopt(arguments, 'r:c:n:s:d:ahv', optionsl)

    return listeopt

def afficher_version():
    print('{} 1.0'.format(sys.argv[0]))


def afficher_aide():
    print('\nUtilisation : {} OPTION…'.format(sys.argv[0]))
    print('Corrige un fichier suivant des règles.\n')

    print('--help, --aide, -h, -a')
    print('              afficher cette aide et quitter')
    print('--version, -v afficher la version et quitter')
    print('--règles=FICHIER, -r FICHER')
    print('              spécifier le fichier contenant les règles de ')
    print('                correction')
    print('-n NOMBRE     spécifier le nombre de mots à traiter en même temps,')
    print('                utile pour les règles portant sur plusieurs mots')
    print('--fichier-à-corriger=FICHIER, -c FICHIER')
    print('              spécifier le fichier à corriger')
    print('--dictionnaire=FICHIER, -d FICHIER')
    print('              spécifier un dictionnaire d’entité')
    print('--sortie=FICHIER, -s FICHIER')
    print('              spécifier un fichier de sortie, ne pas')
    print('                spécifier ou mettre « - » pour utiliser la')
    print('                sortie standard')
    print('--sans-casse  rendre les motifs insensible à la casse')
    print('--ignorer-traits-union')
    print('              les mots liés par un trait d’union sont considérés')
    print('                comme des mots séparés par des espaces')

def traiter_arguments(listeoptions):
    r, c = False, False
    dico = {'n': None, 'casse': True, 'ignorer-traits-union': False}

    for o, a in listeoptions:
        if o in ('-h', '--help', '-a', '--aide'):
            afficher_aide()
            exit(0)
        elif o in ('-v', '--version'):
            afficher_version()
            exit(0)
        elif o in ('--règles', '-r'):
            r = True
            dico['règles'] = a
        elif o in ('--fichier-à-corriger', '-c'):
            c = True
            dico['corriger'] = a
        elif o == '-n':
            dico['n'] = int(a)
        elif o in ('-s', '--sortie'):
            dico['sortie'] = a
        elif o in ('--dictionnaire', '-d'):
            dico['dictionnaire'] = a
        elif o == '--sans-casse':
            dico['casse'] = False
        elif o == '--ignorer-traits-union':
            dico['ignorer-traits-union'] = True

    if not (r and c):
        afficher_aide()
        exit(1)
    else:
        return dico


def extraire_règles(fichier):
    règles = []
    for ligne in fichier:
        règles.append(ligne.strip('\n').split('\t'))
    return règles


def chercher_joker(ligne):
    éléments = ligne.split(';')
    n = 0
    for élément in éléments:
        if élément == '*':
            return (True, n)
        else:
            n += 1
    return (False)


# Permet de changer l’étiquette d’un mot.
def appliquer_règle_type1(liste, règle, dico_options):
    for i in range(len(liste)):
        ligne = liste[i]
        éléments = ligne.strip('\n').split('\t')
        if dico_options['casse']:
            correspondance = re.match(re.escape(règle[1]), éléments[0])
        else:
            correspondance = re.match(re.escape(règle[1]), éléments[0], re.I)
        if correspondance:
            ligne = '{}\t{}\n'.format(éléments[0], règle[2])
            liste[i] = ligne
    return liste

# Permet de créer une phrase à partir d’une liste de lignes avec un
# mot par ligne.
def concaténer_mots(liste):
    phrase = None
    i = 0
    for ligne in liste:
        if ligne != '':
            phrase = '\n' if ligne == '\n' else '{}'.format(ligne.split()[0])
            break
        else:
            i += 1

    if phrase is None:
        raise RuntimeError('Il n’y a aucune ligne correcte dans le fichier à corriger.')

    for ligne in liste[(i + 1):]:
        if ligne == '':
            continue
        elif ligne == '\n':
            phrase += '\n'
        else:
            éléments = ligne.strip('\n').split()
            phrase += ' {}'.format(ligne.split()[0])

    return phrase


# Changer l’étiquette d’un ou plusieurs mots dans un groupe de mots.
def appliquer_règle_type2(liste, règle, dico_options):
    chaîne = concaténer_mots(liste)
    if dico_options['casse']:
        correspondance = re.match(règle[1], chaîne)
    else:
        correspondance = re.match(règle[1], chaîne, re.I)
    if correspondance:
        éléments = règle[2].split(';')
        tous = False
        n = 0
        for élément in éléments:
            if élément == '*':
                tous = True
                break
            else:
                n += 1

        if tous:
            éléments = correspondance[0].split()
            étiquette = règle[3].split(';')[n]
            for i in range(len(éléments)):
                mot = liste[i].strip('\n').split()[0]
                ligne = '{}\t{}\n'.format(mot, étiquette)
                liste[i] = ligne
        else:
            mots = correspondance[0].split()
            chaîne_avant_motif = chaîne[:correspondance.start()]
            indices = [len(chaîne_avant_motif.split())]
            indices.append(indices[0] + len(mots))
            emplacements = règle[2].split(';')
            for i in range(len(emplacements)):
                emplacement = int(emplacements[i])
                if emplacement >= len(mots):
                    raise RuntimeError('La troisième colonne de la règle '\
                                       + repr(règle) + ' fait référence au mot '\
                                       + emplacement + ' mais ce mot n’existe pas.')

                éléments = règle[3].split(';')
                if emplacement < 0 or emplacement > indices[1] - indices[0]:
                    raise RuntimeError('Erreur dans la règle « {} ».'.format(règle))
                elif len(éléments) != len(emplacements):
                    raise RuntimeError('La règle « {} »'.format(règle)\
                                       + ' n’a pas le même nombre de termes '
                                       + 'dans la troisième et la quatrième colonne.')
                étiquette = éléments[i]
                mot = mots[emplacement]
                ligne_actuel = liste[indices[0] + emplacement]
                if ligne_actuel.split()[0] != mot:
                    continue
                elif ligne_actuel.split()[1][2:] == étiquette[2:]\
                     and ligne_actuel.split()[1][:2] == 'I-':
                    continue
                else:
                    liste[indices[0] + emplacement] = '{}\t{}\n'.format(mot, étiquette)

    return liste


def appliquer_règle_type3(liste, règle, dico_options):
    gp = GénérateurParagraphe(liste)
    chaîne = gp.obtenir_chaîne()
    motif = règle[1] + règle[2] + règle[3]
    if dico_options['casse']:
        correspondance = re.search(motif, chaîne)
    else:
        correspondance = re.search(motif, chaîne, re.I)

    if correspondance:
        # Il faut trouver la position dans la liste des mots du motif
        # central et modifier leur étiquette selon la cinquième
        # colonne de la règle.
        if dico_options['casse']:
            correspondance2 = re.search(règle[2], correspondance[0])
        else:
            correspondance2 = re.search(règle[2], correspondance[0], re.I)
        if not correspondance2:
            raise RuntimeError('Erreur inconnu.')

        # Il faut trouver le nombre de mots qu’il y a avant le motif
        # entier.
        position_début = gp.obtenir_position_mot_dans_liste\
            (correspondance.start())

        # Il faut trouver le nombre de mots qu’il y a avant le motif
        # central.
        position_motif_central = gp.obtenir_position_mot_dans_liste\

        # Il faut trouver le nombre de mots qu’il y a dans le motif
        # central.
        position_fin_motif_central = gp.obtenir_position_mot_dans_liste\
            (correspondance2.start() + correspondance.start())
        nombre_de_mots_motif_central = position_fin_motif_central\
            - position_motif_central + 1

        joker = chercher_joker(règle[4])
        if joker:
            étiquette = règle[5].split(';')[joker[1]]
            for i in range(position_motif_central, position_fin_motif_central + 1):
                mot = liste[i].strip('\n').split('\t')[0]
                liste[i] = '{}\t{}\n'.format(mot, étiquette)
        else:
            éléments_position = règle[4].split(';')
            éléments_étiquettes = règle[5].split(';')
            if len(éléments_position) != len(éléments_étiquettes):
                raise RuntimeError('Il n’y a pas le même nombre d’arguments '\
                                   + 'dans la cinquième et la '\
                                   + 'sixième colonne de {}.'.format(règle))
            for i in range(len(éléments_position)):
                position = int(éléments_position[i])
                if int(éléments_position[i]) > len(liste) - 1:
                    raise RuntimeError\
                        ('La règle {} est incorrecte,'.format(règle)\
                         + ' il n’y a pas {} éléments dans {}.'\
                         .format(position + 1, liste))
                else:
                    pos_dans_liste = position + position_motif_central
                    mot = liste[pos_dans_liste].strip('\n').split('\t')[0]
                    liste[pos_dans_liste] = '{}\t{}\n'.format\
                        (mot, éléments_étiquettes[i])

        return liste
    else:
        return liste


# Applique les règles en paramètre à une liste de ligne.
def appliquer_règle(liste, règle, dico_options):
    # Il faut normaliser les mots.
    liste2 = []
    for élément in liste:
        if élément == '' or élément == '\n':
            liste2.append(élément)
        else:
            mot = élément.split('\t')[0]
            reste = '\t'.join(élément.split('\t')[1:])
            chaîne_normalisé = normaliser_mot(mot, dico_options)
            for mot in chaîne_normalisé.split():
                liste2.append(mot + '\t' + reste)

    if int(règle[0]) == 1:
        liste2 = appliquer_règle_type1(liste2, règle, dico_options)
    elif int(règle[0]) == 2:
        liste2 = appliquer_règle_type2(liste2, règle, dico_options)
    elif int(règle[0]) == 3:
        liste2 = appliquer_règle_type3(liste2, règle, dico_options)

    # Fusion contrôlé des listes pour revenir à l’état normal.
    for i in range(len(liste)):
        if liste[i] != liste2[i]:
            mot = liste[i].split('\t')[0]
            étiquette = liste2[i].strip('\n').split('\t')[1]
            liste[i] = '{}\t{}\n'.format(mot, étiquette)
            nb_à_supprimer = len(normaliser_mot(mot, dico_options).split()) - 1
            for j in range(nb_à_supprimer):
                del liste2[i+1]

    return liste


def lire_lignes(fichier, nombre_lignes):
    if nombre_lignes % 2 == 0:
        nombre_lignes += 1

    lignes = []
    lignes.append(fichier.readline())
    position = fichier.tell()
    for i in range(nombre_lignes - 1):
        lignes.append(fichier.readline())
    fichier.seek(position)

    return lignes


def lire_ligne(fichier, nombre_lignes, liste_lignes):
    if len(liste_lignes) > 0:
        liste_lignes.remove(liste_lignes[0])

    liste_lignes.append(fichier.readline())

    return liste_lignes


def lire_début(fichier, nombre_lignes):
    lignes = []
    if nombre_lignes % 2 == 0:
        nombre_lignes += 1
    for i in range(int(nombre_lignes / 2)):
        liste = []
        lignes_vides_ajoutés = int(nombre_lignes / 2) - i
        for j in range(lignes_vides_ajoutés):
            liste.append('')

        position = fichier.tell()
        for j in range(int(nombre_lignes / 2) + (5 - lignes_vides_ajoutés) + 1):
            liste.append(fichier.readline())
        fichier.seek(position)
        lignes.append(liste)

    # Il faut consommer les lignes
    for i in range(1, nombre_lignes):
        fichier.readline()

    return lignes


def appliquer_liste_règles(liste_lignes, règles, dico_options):
    liste = liste_lignes
    liste_courante = liste_lignes
    for règle in règles:
        liste_courante = appliquer_règle(liste_courante, règle, dico_options)
        if liste_courante != liste:
            liste = liste_courante
    return liste


def appliquer_toutes_règles(liste_lignes, règles, dico_options):
    cœurs = cpu_count()
    résultats = []
    lignes_courantes = liste_lignes
    with Pool(cœurs) as groupement:
        for i in range(cœurs):
            début = int((len(règles) / cœurs) * i)
            fin = int((len(règles) / cœurs) * (i + 1))
            résultats.append(groupement.apply_async\
                             (appliquer_liste_règles,
                              (liste_lignes, règles[début:fin],
                               dico_options)))

        lignes_courantes = résultats[0].get()
        for résultat in résultats[1:]:
            lignes = résultat.get()
            if liste_lignes != lignes and lignes_courantes != lignes:
                lignes_courantes = lignes
    return lignes_courantes


def appliquer_règles(fàc, fichier_sortie, règles, dico_options):
    nombre_lignes = dico_options['n']
    indice_courant = int(nombre_lignes / 2)
    liste_liste_lignes = lire_début(fàc, nombre_lignes)
    liste_lignes = []
    for liste_lignes in liste_liste_lignes:
        liste_lignes = appliquer_toutes_règles(liste_lignes, règles,
                                               dico_options)

    if len(liste_liste_lignes) > 0:
        liste_lignes = liste_liste_lignes[len(liste_liste_lignes) - 1]
        liste_lignes = lire_ligne(fàc, nombre_lignes, liste_lignes)
    else:
        liste_lignes = lire_lignes(fàc, nombre_lignes)

    while liste_lignes[indice_courant] != '':
        liste_lignes = appliquer_toutes_règles(liste_lignes, règles,
                                               dico_options)

        fichier_sortie.write(liste_lignes[0])
        liste_lignes = lire_ligne(fàc, nombre_lignes, liste_lignes)

    for ligne in liste_lignes[1:]:
        fichier_sortie.write(ligne)


def obtenir_fichier_sortie(dico):
    if 'sortie' not in list(dico.keys()) or dico['sortie'] == '-':
        return sys.stdout
    else:
        return open(dico['sortie'], 'w')


def transformer_dictionnaire_en_règles(fichier):
    règles = []

    for indice, ligne in enumerate(fichier):
        éléments = ' '.join(ligne.strip('\n').split()).split('=>')
        for i in range(len(éléments)):
            éléments[i] = éléments[i].strip()

        if len(éléments) != 2:
            sys.stderr.write\
                ('La ligne {} du dictionnaire est incorrecte.\n'.format\
                 (indice + 1))

        entité = éléments[0]
        étiquette = éléments[1]

        éléments = entité.split()
        positions = '0'
        étiquettes = 'B-' + étiquette
        for i in range(1, len(entité.split())):
            mot = re.escape(entité[i])
            positions += ';{}'.format(i)
            étiquettes += ';I-{}'.format(étiquette)

        règle = '2\t{}\t{}\t{}'.format(entité, positions, étiquettes).split('\t')
        règles.append(règle)

    return règles


def normaliser_mot(mot, dico_options):
    if dico_options['ignorer-traits-union']:
        mot2 = mot
        mot = ' '.join(mot.split('-'))
        if mot.strip() == '':
            return mot2

    return mot


def principale(arguments):
    locale.setlocale(locale.LC_ALL, '')
    listeoptions = lire_arguments(arguments[1:])
    résultat_traitement = traiter_arguments(listeoptions)

    if 'dictionnaire' in list(résultat_traitement.keys()):
        règles = transformer_dictionnaire_en_règles\
            (open(résultat_traitement['dictionnaire'], 'r'))
    else:
        règles = []

    with open(résultat_traitement['règles'], 'r') as f:
        règles += extraire_règles(f)

    with open(résultat_traitement['corriger'], 'r') as fàc:
        if résultat_traitement['n'] is None:
            résultat_traitement['n'] = 1

        sortie = obtenir_fichier_sortie(résultat_traitement)
        appliquer_règles(fàc, sortie, règles, résultat_traitement)


if __name__ == '__main__':
    principale(sys.argv)
