#!/usr/bin/env python3

# Auteur : Corentin Bocquillon <corentin@nybble.fr> 2018

# Ce logiciel est régi par la licence CeCILL soumise au droit français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
# sur le site « http://www.cecill.info ».

# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.

# A cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement,
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
# pris connaissance de la licence CeCILL, et que vous en avez accepté les
# termes.

from sys import argv
from sys import stdout

with open(argv[1], 'r') as fichier_2col:
    with open(argv[2], 'r') as fichier_3col:
        for ligne2col in fichier_2col:
            ligne3col = fichier_3col.readline()
            éléments2col = ligne2col.strip('\n').split()
            éléments3col = ligne3col.strip('\n').split()
            if (len(éléments3col) != 3 or len(éléments2col) != 2)\
               and (ligne3col != '\n' or ligne2col != '\n'):
                continue
            elif ligne2col == '\n':
                if ligne3col != '\n':
                    continue
                else:
                    stdout.write(ligne2col)
            else:
                stdout.write('{} {} {}\n'.format(éléments2col[0],
                                                 éléments3col[1],
                                                 éléments2col[1]))
